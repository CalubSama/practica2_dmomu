import React, { Component } from 'react';
import {AppRegistry, ScrollView,} from 'react-native'
import SingleView from './components/SingleView.js'
import Splash from './components/Splash.js'
import ListView from './components/ListView.js'
import Home from './components/Home.js'
import About from './components/About.js'
import Routes from './components/Routes.js'


class NavegacionPF extends Component{
  render(){
    return(
      <>
      <Splash/>
      <Routes/>
      </>
    )
  }
}

export default App;

AppRegistry.registerComponent('NavegacionPF', ()=>NavegacionPF)