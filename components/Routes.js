import React from 'react'
import {Router, Scene} from 'react-native-router-flux'
import About from './About.js'
import Home from './Home.js'
import ListView from './ListView.js'
import SingleView from './SingleView.js'
import Splash from './Splash.js'

const Routes = ()=>(
    <Router>
        <Scene key='root'>
            <Scene key='Splash' component={Splash} title='Splash' initial={true}/>
            <Scene key='Home' component={Home} title='Home'/>
            <Scene key='Listview' component={ListView} title='ListView'/>
            <Scene key='Singleview' component={SingleView} title='SingleView'/>
            <Scene key='About' component={About} title='About'/>
        </Scene>
    </Router>
)

export default Routes;