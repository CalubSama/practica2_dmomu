import React,{ Component } from 'react'
import { Image, View, StyleSheet, StatusBar, TouchableOpacity} from 'react-native'
import { Actions } from 'react-native-router-flux'

class Splash extends Component{

    render(){
        return(
            <>
                <StatusBar barStyle="dark-content" hidden={false} translucent={true} backgroundColor="#A3D5CB"/>
                <View style={styles.container}>
                    <TouchableOpacity onPress={goToHome}>
                        <View style={styles.imgContainer}>
                            <Image source={require('../assets/gooDev2.png')} style={styles.logo}/>
                        </View>
                    </TouchableOpacity>
                </View>
            </>
        )
    }

}

 export default Splash

 const styles= StyleSheet.create({
     container:{
        flexDirection:"column",
        justifyContent: "center",
        alignItems: "center",
        height: "100%",
        backgroundColor: "#A3D5CB",
     },

     imgContainer:{
        flex: 1,
        justifyContent: "center",
        height: "100%"
     },

     logo:{
         width: 200,
         height: 200,
     }
 })