import React,{Component} from 'react'
import {Text, TouchableOpacity, View, StyleSheet, StatusBar, ScrollView} from 'react-native'

class ListView extends Component{

    state= {
        data:[]
    }

    componentDidMount= () => {
        fetch('https://jsonplaceholder.typicode.com/posts', {method: 'GET'})
        .then((response)=>response.json())
        .then((responseJson)=>{
            console.log(responseJson);
            this.setState({
                data: responseJson
            })
        })
        .catch((error)=>{
            console.error(error);
        });
    }

    alertTitle=(item)=>{
        alert(item.userId)
    }

    render(){
        return(
            <>
                <ScrollView>
                <StatusBar barStyle = "dark-content" hidden = {false} backgroundColor="#A3D5CB" translucent = {true}/>
                <View style={styles.container}>
                    {
                        this.state.data.map((item, index)=>(
                            <TouchableOpacity key={item.id} onPress={() => this.alertTitle(item)}>
                                <Text>{'TITLE: '}{item.title}{'\n'}</Text>
                            </TouchableOpacity>
                        ))
                    }
                </View>
                </ScrollView>
            </>
        )
    }
}

export default ListView

const styles= StyleSheet.create({
    container:{
        backgroundColor:"#4169e1",
    },
})