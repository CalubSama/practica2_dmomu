import React, {Component} from 'react'
import {Text, View, StyleSheet, TouchableOpacity, Button, StatusBar} from 'react-native'

class SingleView extends Component{
    state= {
        data:''
    }

    componentDidMount= () => {
        fetch('https://jsonplaceholder.typicode.com/posts/4', {method: 'GET'})
        .then((response)=>response.json())
        .then((responseJson)=>{
            console.log(responseJson);
            this.setState({
                data: responseJson
            })
        })
        .catch((error)=>{
            console.error(error);
        });
    }

    render(){
        return(
            <>
                <StatusBar barStyle = "dark-content" hidden = {false} backgroundColor = "#A3D5CB" translucent = {true}/>
                <View style={styles.container}>
                    <Text style={style.textSt}>{'USER ID: '}{this.state.data.userId}{'\n'}</Text>
                    <Text> style={style.textSt}{'ID: '}{this.state.data.id}{'\n'}</Text>
                    <Text> style={style.textSt}{'TITLE: '}{this.state.data.title}{'\n'}</Text>
                    <Text> style={style.textSt}{'BODY: '}{this.state.data.body}{'\n'}</Text>
                </View>
            </>
        )
    }

}

export default SingleView;

const styles= StyleSheet.create({
    container:{
        flex:1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        height:100,
        backgroundColor:"#4169e1",
    },
    textSt:{
         fontSize:25,
         width:200,
         height:255,
         textAlign:'center',
         paddingTop:80,
        paddingLeft:60
    },
})