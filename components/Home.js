import React, { Component } from 'react';
import {StyleSheet, View, Text, StatusBar,} from 'react-native';

class Home extends Component{
    render(){
        return(
            <>
            <StatusBar barStyle = "dark-content" hidden = {false} backgroundColor = "#A3D5CB" translucent = {true}/>
            <View style={styles.container}>
                    <View style={styles.container2}>
                        <View>
                            <View>
                                <TouchableOpacity onPress={goToListView}>
                                <Text style={style.textSt}> ListView </Text>
                                </TouchableOpacity>
                            </View>
                            <View>
                                <TouchableOpacity onPress={goToSingleView}>
                                <Text style={style.textSt}> SingleView </Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                        <View>
                            <View>
                                <TouchableOpacity onPress={goToAbout}>
                                <Text style={style.textSt}> About </Text>
                                </TouchableOpacity>
                            </View>
                            <View>
                                <TouchableOpacity onPress={goToSplash}>
                                <Text style={style.textSt}> Splash </Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </View>
            </>
        )
    }
}

export default Home

const goToSingleView=()=>{
    Actions.SingleView()
}

const goToListView=()=>{
    Actions.ListView()
}

const goToAbout=()=>{
    Actions.About()
}

const goToSplash=()=>{
    Actions.Splash()
}

const styles= StyleSheet.create({
    container:{
        flex:1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        height:100,
        backgroundColor:"#4e136e",
    },

    container2:{
        marginTop:50,
        flex:0.7,
        flexDirection: 'row',
        height:510,
        width:510,
        backgroundColor:"#4169e1",
        margin: 10,
     },
     textSt:{
     	fontSize:25,
     	width:200,
     	height:255,
     	textAlign:'center',
     	paddingTop:80,
		paddingLeft:60
	},
})