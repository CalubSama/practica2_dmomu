import React,{Component} from 'react'
import {Text, Image, View, StyleSheet, StatusBar, ScrollView} from 'react-native'

class About extends Component{
    render(){
        return(
            <>
                <ScrollView>
                <StatusBar barStyle="dark-content" hidden={false} translucent={true} backgroundColor="#A3D5CB"/>
                <View style={styles.container}>
                    <View style={styles.imgContainer}>
                        <Image source={require('../assets/gooDev2')} style={styles.logo}/>
                    </View>
                    <Text style={styles.nombre}>
                        Terán Díaz Jehú Caleb{'\n'}
                        Terán Díaz Jehú Caleb{'\n'}
                        Terán Díaz Jehú Caleb{'\n'}
                        Terán Díaz Jehú Caleb{'\n'}
                        Terán Díaz Jehú Caleb{'\n'}
                        Terán Díaz Jehú Caleb{'\n'}
                        Terán Díaz Jehú Caleb{'\n'}
                        Terán Díaz Jehú Caleb{'\n'}
                        Terán Díaz Jehú Caleb{'\n'}
                        Terán Díaz Jehú Caleb{'\n'}
                    </Text>
                </View>
                </ScrollView>
            </>
        )
    }
}

export default About

const styles= StyleSheet.create({
    container:{
       flexDirection:"column",
       justifyContent: "center",
       alignItems: "center",
       height: "100%",
       backgroundColor: "#4e136e",
    },

    imgContainer:{
       flex: 1,
       justifyContent: "flex-start",
       height: "100%"
    },

    logo:{
        width: 270,
        height: 200,
    },

    nombre:{
        height:470,
        color: '#A3D5CB',
        textAlign: 'center',
    },


})